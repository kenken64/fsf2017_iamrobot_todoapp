# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Functionalities covered:
●     Add Todo

●     Edit Todo ( Due date , task description … etc)

●     Set due date for your todos

●     List multiple to do items

●     Delete ToDo - Only pending todos

●     Create category

●     Assign To do item to a category

●     Search Todo based on category

●     Notify the person’s email that it's to do about to due one day before the due date.(BONUS)

●     Show different segment of the pending todos and completed todos. (BONUS)

### Planned Layout diagram ###
![Groupwork_TodoApp.jpg](https://bitbucket.org/repo/75Mpkd/images/1870422884-Groupwork_TodoApp.jpg)


### Who do I talk to? ###

Team Members:
- Chan Ah Boon
- Tan Chee Seong
- Christian Nugraha