var path = require('path');

module.exports = function(app, todoDB) {

    // GET TASK 
    app.get("/tasks", function(req, resp) {
        console.log(">> Getting Task");
        var whereClause = {};
        if (req.query.duedate) {
            whereClause.duedate = new Date(req.query.duedate);
        }
        console.log(req.query.duedate);
        todoDB.Task
            .findAll({
                where: whereClause, 
                include: [{
                    model: todoDB.TaskCategory, 
                    limit: 1,
                    include: [{
                        model: todoDB.Category
                    }]
                }]
            }).then(function(tasks) {
                for (var i in tasks) {
                    tasks[i].dataValues.duedate = new Date(tasks[i].dataValues.duedate);
                    tasks[i].dataValues.category = 
                        tasks[i].dataValues.taskcats[0].category;
                    delete tasks[i].dataValues.taskcats;
                }
                resp
                    .status(200)
                    .json(tasks);
            }).catch(function(err){
                resp
                    .status(500)
                    .json(err);
            });
    });

    // INSERT TASK
    app.post("/tasks", function(req, resp) {
        console.log("INSERT TASK");
        //console.log(req);
        console.log(req.body.duedate);
        console.log(req.body.category);
        console.log(req.body.task_title);
        console.log(req.body.task_desc);

        var newDueDate  = new Date(req.body.duedate);
        newDueDate.setHours(0,0,0,0);

        todoDB.Task
            .create({
                duedate: newDueDate , 
                task_status:0, 
                task_title: req.body.task_title, 
                task_desc: req.body.task_desc
            }).then(function(task){
                console.log(task.dataValues);
                todoDB.TaskCategory
                    .create({
                        task_id: task.dataValues.task_id,
                        cat_id: req.body.category
                    }).then(function(taskCat) {
                        console.log(taskCat.dataValues);
                        resp
                            .status(200)
                            .json(task.dataValues);
                    }).catch(function(err) {
                        console.log(err);
                        resp
                            .status(500)
                            .json(err);
                    });       
            }).catch(function(err){
                console.log(err);
                resp
                    .status(500)
                    .json(err);
            });
    });


    app.put("/tasks/:task_id", function(req, resp) {
        console.log("UPDATE TASK");
        //console.log(req);
        console.log(req.body.duedate);
        console.log(req.body.category);
        console.log(req.body.task_title);
        console.log(req.body.task_desc);
        console.log(req.body.task_status);

        var newDueDate  = new Date(req.body.duedate);
        newDueDate.setHours(0,0,0,0);

        var updateInfo = {};

        if (req.body.task_title) {updateInfo.task_title = req.body.task_title};
        if (req.body.task_desc) {updateInfo.task_desc = req.body.task_desc};
        if (!isNaN(req.body.task_status)) {updateInfo.task_status = req.body.task_status};
        if (req.body.duedate) {
            var newDueDate  = new Date(req.body.duedate);
            newDueDate.setHours(0,0,0,0);
            updateInfo.duedate = newDueDate;
        };

        var where = {};
        where.task_id = req.params.task_id;

        todoDB.Task
            .update(
                updateInfo,
                {where: where
            }).then(function(result){
                console.log(result);
                if (req.body.category) {
                    todoDB.TaskCategory
                        .update({
                                cat_id: req.body.category
                            }, {where:where
                        }).then(function(result) {
                            resp
                                .status(200)
                                .json({success:true});
                        }).catch(function(err) {
                            console.log(err);
                            resp
                                .status(500)
                                .json(err);
                        });
                }  else {
                    resp
                        .status(200)
                        .json({success:true}); 
                }       
            }).catch(function(err){
                console.log(err);
                resp
                    .status(500)
                    .json(err);
            });
    });

    app.delete("/tasks/:task_id", function(req, resp) {
        console.log("DeleteTask");
        console.log(req.params.task_id);
        var whereClause = {};
        whereClause.task_id = req.params.task_id;

        todoDB.TaskCategory
            .destroy({
                where: whereClause
            }).then(function(result) {
                console.log(result);
                if (result == 1) {
                    todoDB.Task
                        .destroy({
                            where: whereClause
                        }).then(function(result){
                            if (result == 1) {
                                resp
                                .status(200)
                                .json({success:true});
                            } else {
                                resp
                                .status(200)
                                .json({success:false});
                            }
                        })
                } else {
                    resp
                        .status(200)
                        .json({success:false});
                }
            }).catch(function(err){
                resp
                    .status(500)
                    .json(err);
            });
    });


    // INSERT Category
    app.post("/categories", function(req, resp) {
        console.log("INSERT Category");
        //console.log(req);
        console.log(req.body.cat_name);

        todoDB.Category
            .findAll({
                where: {cat_name: {$like: req.body.cat_name}}
            }).then(function(category){
                //console.log(category);
                if (category.length == 0) {
                    todoDB.Category
                        .create({
                            cat_name: req.body.cat_name
                        }).then(function(newcategory) {
                            resp
                            .status(200)
                            .json(newcategory);
                        }).catch(function(err){
                            console.log(err);
                            resp
                                .status(500)
                                .json(err);
                        });
                } else {
                    resp.status(406);
                    resp.end();
                }
            }).catch(function(err){
                console.log(err);
                resp
                    .status(500)
                    .json(err);
            });
    });

    app.get("/categories", function(req, resp) {
        console.log("GET Category");
        
        todoDB.Category
            .findAll({   
            }).then(function(category){    
                resp
                .status(200)
                .json(category);
            }).catch(function(err){
                console.log(err);
                resp
                    .status(500)
                    .json(err);
            });
    });
}