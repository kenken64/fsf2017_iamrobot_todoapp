(function() {
    angular
        .module("toDoListApp")
        .controller("NewTaskCtrl", NewTaskCtrl);

    NewTaskCtrl.$inject = ["$scope", "TaskService","todoSharedService"];

    function NewTaskCtrl($scope, TaskService, todoSharedService) {
        var newTaskCtrl = this;

        newTaskCtrl.updateCategory = function () {
            TaskService.retrieveCategory()            
                .then(function (result) {
                    newTaskCtrl.categorizedTasks = result;
                });
        }

        newTaskCtrl.initField = function() {
            newTaskCtrl.task_title = "";
            newTaskCtrl.category = "";
            newTaskCtrl.task_desc = "";
            newTaskCtrl.categorizedTasks = [];
            newTaskCtrl.dueDate = new Date();
            newTaskCtrl.showForm = false;
            newTaskCtrl.updateCategory();
        }

        newTaskCtrl.initField();

        newTaskCtrl.submitNewTask = function () {
            //console.log("New Task\ntitle:%s\ncategory: %s\nDescription: %s\n:Due Date:%s", 
            //newTaskCtrl.task_title, newTaskCtrl.category, newTaskCtrl.task_desc, newTaskCtrl.dueDate);

            TaskService.insertTask(
                { 
                    task_title: newTaskCtrl.task_title,
                    category: newTaskCtrl.category,
                    task_desc: newTaskCtrl.task_desc,
                    duedate: newTaskCtrl.dueDate
                }
            ).then(function (result) {
                console.log("Task added!!");
                //console.log(result.data);
                result.duedate = new Date(result.duedate);
                todoSharedService.broadcastNewTaskAdded(result);
            }).catch(function () {
                console.log("Add Task FAIL!!");
            }).finally(function () {
                newTaskCtrl.initField();
            });
        }

        newTaskCtrl.closeForm = function () {
            newTaskCtrl.showForm = false;
        }

        $scope.$on('handleCatBroadcast', function () {
            newTaskCtrl.updateCategory();
            newTaskCtrl.category = "";
        })

        $scope.$on('newTaskFrmReq', function () {
            newTaskCtrl.showForm = true;
        })
    }
})();