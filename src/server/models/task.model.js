module.exports = function(connection, Sequelize) {
    var Task = connection.define(
        "task", {
            task_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            duedate: {
                type: Sequelize.DATE,            
                primaryKey: false,
                allowNull: false
            },
            task_status: {
                type: Sequelize.INTEGER,
                primaryKey: false,
                allowNull: false
            },
            task_title: {
                type: Sequelize.STRING,
                primaryKey: false,
                allowNull: false
            },
            task_desc: {
                type: Sequelize.STRING(1000),
                primaryKey: false,
                allowNull: false
            }
        },{
            tableName: 'tasks',
            timestamps: false
        }
    );
    return Task;
}