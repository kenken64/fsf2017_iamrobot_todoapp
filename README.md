# README #

To Do App

Group Name: IamRobot
   * Tan Chee Seong (tcheeseo)  Coder 
   * Christian Nugraha (luk45) Solution
   * Chan Ah Boon UX

Features

●     Add Todo

●     Edit Todo ( Due date , task description … etc)

●     Set due date for your todos

●     List multiple to do items

●     Delete ToDo - Only pending todos

●     Create category

●     Assign To do item to a category

●     Search Todo based on category

●     Notify the person’s email that it's to do about to due one day before the due date.(BONUS)

●     Show different segment of the pending todos and completed todos. (BONUS)


![Groupwork_TodoApp.jpg](https://bitbucket.org/repo/pxRLBa/images/2352014532-Groupwork_TodoApp.jpg)