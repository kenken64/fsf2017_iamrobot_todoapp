(function() {
    angular
        .module("toDoListApp")
        .controller("EditTaskCtrl", EditTaskCtrl);

    EditTaskCtrl.$inject = ["$scope", "TaskService","todoSharedService"];

    function EditTaskCtrl($scope, TaskService, todoSharedService) {
        var editTaskCtrl = this;

        editTaskCtrl.categorizedTasks = [];

        editTaskCtrl.updateCategory = function () {
            TaskService.retrieveCategory()
                .then(function (result) {
                    editTaskCtrl.categorizedTasks = result;
                });
        }

        editTaskCtrl.initField = function() {
            editTaskCtrl.task_id = -1;
            editTaskCtrl.duedate = new Date();
            editTaskCtrl.category = "";
            editTaskCtrl.task_title = "";
            editTaskCtrl.task_desc = "";
            editTaskCtrl.showForm = false;
            editTaskCtrl.updateCategory();
        }
        editTaskCtrl.initField();

        editTaskCtrl.submitEditTask = function () {
            TaskService.updateTask(
                { 
                    task_id: editTaskCtrl.task_id,
                    task_title: editTaskCtrl.task_title,
                    category: editTaskCtrl.category,
                    task_desc: editTaskCtrl.task_desc,
                    duedate: editTaskCtrl.duedate
                }
            ).then(function (result) {
                console.log("Task Update!!");
                //console.log(result);
                result.duedate = new Date(result.duedate);
                todoSharedService.broadcastUpdateTaskDetail();
            }).catch(function () {
                console.log("Edit Task FAIL!!");
            }).finally(function () {
                editTaskCtrl.showForm = false;
            });
            
        }

        editTaskCtrl.closeForm = function () {
            editTaskCtrl.showForm = false;
        }

        $scope.$on('handleCatBroadcast', function () {
            editTaskCtrl.updateCategory();
            editTaskCtrl.category = "";
        });

        $scope.$on('editTask', function (event, currentTask) {
            //console.log(currentTask);
            editTaskCtrl.task_id = currentTask.task_id;
            editTaskCtrl.duedate = currentTask.duedate;
            editTaskCtrl.category = currentTask.category.cat_id + "";
            editTaskCtrl.task_title = currentTask.task_title;
            editTaskCtrl.task_desc = currentTask.task_desc;
            editTaskCtrl.showForm = true;
            window.scrollTo(0, 0);
        })
    }
})();