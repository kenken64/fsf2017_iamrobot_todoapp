(function() {
    angular
        .module("toDoListApp")
        .controller("TomorrowTaskCtrl", TomorrowTaskCtrl);

    TomorrowTaskCtrl.$inject = ["$scope", "TaskService","todoSharedService"];

    function TomorrowTaskCtrl($scope, TaskService, todoSharedService) {
        var tomorrowTaskCtrl = this;

        tomorrowTaskCtrl.taskList = [];

        tomorrowTaskCtrl.tomorrow = new Date();
        tomorrowTaskCtrl.tomorrow.setHours(0,0,0,0);
        tomorrowTaskCtrl.tomorrow.setDate(tomorrowTaskCtrl.tomorrow.getDate() + 1);

        tomorrowTaskCtrl.selectedCategory = "";
        tomorrowTaskCtrl.selectedTaskCategory = "";

        tomorrowTaskCtrl.updateTaskList = function() {
            TaskService.retrieveTask(tomorrowTaskCtrl.tomorrow)
                .then(function (result) {
                    tomorrowTaskCtrl.taskList = result;
                    for (var i = 0; i < tomorrowTaskCtrl.taskList.length; i++) {
                        tomorrowTaskCtrl.taskList[i].duedate = new Date(tomorrowTaskCtrl.taskList[i].duedate);
                        tomorrowTaskCtrl.taskList[i].read = "read more";
                        tomorrowTaskCtrl.taskList[i].readMore = false;
                        tomorrowTaskCtrl.taskList[i].confirmComplete = false;
                    }
                });
        }
        tomorrowTaskCtrl.updateTaskList();

        tomorrowTaskCtrl.handleReadMoreLess = function (task_id) {
            var idx = tomorrowTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            tomorrowTaskCtrl.taskList[idx].readMore = !tomorrowTaskCtrl.taskList[idx].readMore;
            tomorrowTaskCtrl.taskList[idx].read = (tomorrowTaskCtrl.taskList[idx].readMore) ? "read less" : "read more";
        }

        tomorrowTaskCtrl.handleEditTask = function (task_id) {
            var idx = tomorrowTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            if (idx != -1) {
                todoSharedService.broadcastEditTask(tomorrowTaskCtrl.taskList[idx]);
            }        
        };

        tomorrowTaskCtrl.handleRemoveTask = function (task_id) {
            var idx = tomorrowTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            if (idx != -1) {
                TaskService.removeTask(task_id)
                    .then(function (response) {
                        tomorrowTaskCtrl.taskList.splice(idx, 1);
                        todoSharedService.broadcastDeleteTomorrowTask();
                    });
            }
        }

        //NEW COMPLETE TASK
        tomorrowTaskCtrl.showConfirmComplete = function(task_id, state) {
            var idx = tomorrowTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            console.log("Index : %s ", task_id);
            tomorrowTaskCtrl.taskList[idx].confirmComplete = state;
        }

        tomorrowTaskCtrl.handleCompleteTask = function(task_id) {
            var idx = tomorrowTaskCtrl.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            if (idx != -1) {
                TaskService.completeTask(task_id)
                    .then(function(response) {
                        todoSharedService.broadcastUpdateTaskDetail();
                        console.log("item successfully completed" );
                    });        
            }
            tomorrowTaskCtrl.showConfirmComplete(task_id, false);
        }

        
        $scope.$on('newTaskAdded', function (event, newTask) {
            if (tomorrowTaskCtrl.tomorrow.getTime() == newTask.duedate.getTime()) {
                console.log("Got A NEW Task for tomorrow >> ");
                tomorrowTaskCtrl.updateTaskList();
            }
        });

        $scope.$on('deleteTask', function () {
            tomorrowTaskCtrl.updateTaskList();
        });

        $scope.$on('updateTaskDetail', function () {        
            tomorrowTaskCtrl.updateTaskList();
        });

        $scope.$on('handleSelCategoryBroadcast', function (event, selCategory) {
            console.log(selCategory);
            tomorrowTaskCtrl.selectedCategory = selCategory;
        });

         $scope.$on('handleSelTaskCategoryBroadcast', function (event,selTaskCategory) {
            console.log(selTaskCategory);
            tomorrowTaskCtrl.selectedTaskCategory = selTaskCategory.filterValue;
        });
    }
})();