(function () {
    angular
        .module("toDoListApp")
        .factory("todoSharedService", todoSharedService);

        //todoSharedService.$inject = ['$rootScope'];

        function todoSharedService($rootScope) {
            service = {};

            service.broadcastNewCategoryFrmReq = broadcastNewCategoryFrmReq;
            service.broadcastNewTaskFrmReq = broadcastNewTaskFrmReq;
            service.broadcastNewCategory = broadcastNewCategory;
            service.broadcastSelCategory = broadcastSelCategory;
            service.broadcastSelTaskCategory = broadcastSelTaskCategory;
            service.broadcastNewTaskAdded = broadcastNewTaskAdded;
            service.broadcastDeleteTask = broadcastDeleteTask;
            service.broadcastEditTask = broadcastEditTask;
            service.broadcastDeleteTodayTask = broadcastDeleteTodayTask;
            service.broadcastDeleteTomorrowTask = broadcastDeleteTomorrowTask;
            service.broadcastUpdateTaskDetail = broadcastUpdateTaskDetail;

            return service;

            ////////////////////

            function broadcastNewCategoryFrmReq () {
                $rootScope.$broadcast('newCategoryFrmReq');
            };

            function broadcastNewTaskFrmReq () {
                $rootScope.$broadcast('newTaskFrmReq');
            };

            function broadcastNewCategory () {
                $rootScope.$broadcast('handleCatBroadcast');
            };

            function broadcastSelCategory (selCategory) {
                $rootScope.$broadcast('handleSelCategoryBroadcast', selCategory);
            };

            function broadcastSelTaskCategory (selTaskCategory) {
                $rootScope.$broadcast('handleSelTaskCategoryBroadcast', selTaskCategory);
            };

            function broadcastNewTaskAdded (newTask) {
                $rootScope.$broadcast('newTaskAdded', newTask);
            };

            function broadcastDeleteTask (removedTask) {
                $rootScope.$broadcast('deleteTask', removedTask);
            };

            function broadcastEditTask (currentTask) {
                $rootScope.$broadcast('editTask', currentTask);
            };

            function broadcastDeleteTodayTask () {
                $rootScope.$broadcast('deleteTodayTask');
            };

            function broadcastDeleteTomorrowTask () {
                $rootScope.$broadcast('deleteTomorrowTask');
            };

            function broadcastUpdateTaskDetail () {
                $rootScope.$broadcast('updateTaskDetail');
            };
        }
})();
