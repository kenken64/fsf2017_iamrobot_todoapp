module.exports = function() {
    return {
        taskList : [],
        categoryList : [],

        initDummyRecord : function() {
            var today = new Date();
            today.setHours(0,0,0,0);

            var yesterday = new Date();
            yesterday.setHours(0,0,0,0);
            yesterday.setDate(yesterday.getDate() - 1);

            var tomorrow = new Date();
            tomorrow.setHours(0,0,0,0);
            tomorrow.setDate(tomorrow.getDate() + 1);
            
            this.taskList.push({task_id: 1, duedate: yesterday , task_status:0, category: {cat_id: 3, cat_name: "Work"}    , task_title: "NUS-ISS class", task_desc: "Now use Lorem Ipsum as their default model text, and a search"});
            this.taskList.push({task_id: 2, duedate: today     , task_status:0, category: {cat_id: 2, cat_name: "Family"}  , task_title: "Search@#$ function TEST", task_desc: "It is a long act that a reader will be distracted by the readable content of a page when looking at its layout."});
            this.taskList.push({task_id: 3, duedate: today     , task_status:1, category: {cat_id: 2, cat_name: "Family"}  , task_title: "Family dinner", task_desc: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout."});
            this.taskList.push({task_id: 4, duedate: today     , task_status:0, category: {cat_id: 3, cat_name: "Work"}    , task_title: "Meeting @ work", task_desc: "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search"});
            this.taskList.push({task_id: 5, duedate: today     , task_status:1, category: {cat_id: 3, cat_name: "Work"}    , task_title: "Project agile", task_desc: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin years old."});
            this.taskList.push({task_id: 6, duedate: tomorrow  , task_status:0, category: {cat_id: 2, cat_name: "Family"}  , task_title: "Kids tuition $$", task_desc: "Random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old."});
            this.taskList.push({task_id: 7, duedate: tomorrow  , task_status:0, category: {cat_id: 1, cat_name: "Personal"}, task_title: "Appointment ##", task_desc: "Random text. It has rclassical Latin literature from 45 BC, Lorem Ipsum is not simext. Making it over 2000 years old."});
            this.taskList.push({task_id: 8, duedate: yesterday , task_status:0, category: {cat_id: 2, cat_name: "Family"}  , task_title: "Family outing", task_desc: "Random text. It has roots in a piece of Ipsum is not simply random text. Making it over 2000 years old."});
            this.taskList.push({task_id: 9, duedate: today     , task_status:0, category: {cat_id: 3, cat_name: "Work"}    , task_title: "** Boss meeting!! **", task_desc: "Now use Lorem Ipsum as their default model text, and a search"});

            this.categoryList.push({cat_id: 1, cat_name: "Personal"});
            this.categoryList.push({cat_id: 2, cat_name: "Family"});
            this.categoryList.push({cat_id: 3, cat_name: "Work"});
        },


        addTask : function(newTask) {
            newTask.task_id = new Date().getTime();
            var newCategory = {cat_id:0, cat_name:'Others'};
            for (var i in this.categoryList) {
                if (this.categoryList[i].cat_id == newTask.category) {
                    newTask.category = this.categoryList[i];
                    break;
                }
            }
            this.taskList.push(newTask);
        },

        existCategory : function(newCategory) {
            for (var i in this.categoryList) {
                if (this.categoryList[i].cat_name.toLowerCase() == newCategory.toLowerCase()) {
                    return true;
                }
            }
            return false;
        },

        addCategory : function(newCategory) {
            var newCat_id = new Date().getTime();
            var newCategory = {cat_id: newCat_id, cat_name: newCategory};

            this.categoryList.push(newCategory);

            return newCategory;
        },

        getTaskList : function(checkDate) {
            var resultArr = [];
            if (checkDate) {
                for (i in this.taskList) {
                    if (this.taskList[i].duedate.getTime() == checkDate.getTime()) {
                        resultArr.push(this.taskList[i]);
                    }
                }
                return resultArr;
            }
            return this.taskList;
        },

        removeTask : function (task_id) {
            var idx = this.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            if (idx != -1) {
                this.taskList.splice(idx, 1);
                return true;
            }
            return false;
        },
        
        completeTask : function (task_id) {
            var idx = this.taskList.findIndex(function (elem) {
                return elem.task_id == task_id;
            });
            if (idx != -1) {
                this.taskList[idx].task_status=1;
                return true;
            }
            return false;
        },

        editTask : function (currentTask) {
            var idx = this.taskList.findIndex(function (elem) {
                return elem.task_id == currentTask.task_id;
            });
            if (idx != -1) {
                this.taskList[idx].duedate = currentTask.duedate;

                for (var i in this.categoryList) {
                    if (this.categoryList[i].cat_id == currentTask.category) {
                        this.taskList[idx].category  = this.categoryList[i];
                        break;
                    }
                }

                this.taskList[idx].task_title = currentTask.task_title;
                this.taskList[idx].task_desc = currentTask.task_desc;
                return true;
            }
            return false;
        }
    }
}